#!/bin/bash

rm -f servip.sql
# Root files
cat schema.sql >> servip.sql
# Folders
#cat insertions/*.sql >> servip.sql
#cat functions/*.sql >> servip.sql
#cat triggers/*.sql >> servip.sql

sudo su -c 'cp init.sql /var/lib/postgresql/ && cp servip.sql /var/lib/postgresql'

echo "Creating DB..."
sudo su - postgres -c 'psql -U postgres -d postgres -a -f init.sql'

echo "Setting up DB..."
sudo su - postgres -c 'psql -U postgres -d servip -a -f servip.sql'
