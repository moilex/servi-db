DO
$do$
BEGIN
  IF EXISTS (SELECT 1 FROM pg_database WHERE datname = 'servip') THEN

      RAISE NOTICE 'Database already exists';

  ELSE

    CREATE EXTENSION IF NOT EXISTS dblink;
    PERFORM dblink_exec('dbname=' || current_database()  -- current db
    , 'CREATE DATABASE servip;');
    PERFORM dblink_exec('dbname=servip', 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
    PERFORM dblink_exec('dbname=servip', 'CREATE EXTENSION IF NOT EXISTS "pgcrypto";');
    DROP EXTENSION dblink;

  END IF;
END
$do$;
