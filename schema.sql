
------------------------------------------
-------- COUNTRY TABLES ISSUES -----------
------------------------------------------

DROP TABLE IF EXISTS country CASCADE;
CREATE TABLE country (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , name VARCHAR(50) NOT NULL
  , code VARCHAR(3) NOT NULL
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
);
ALTER TABLE country OWNER TO postgres;
GRANT ALL ON TABLE country TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE country TO webuser;

DROP TABLE IF EXISTS city CASCADE;
CREATE TABLE city (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , country_id  UUID NOT NULL REFERENCES country(id)
  , name VARCHAR(50) NOT NULL
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
);
ALTER TABLE city OWNER TO postgres;
GRANT ALL ON TABLE city TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE city TO webuser;

------------------------------------------
---------- USER TABLES ISSUES ------------
------------------------------------------

DROP TABLE IF EXISTS user_state CASCADE;
CREATE TABLE user_state (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , name VARCHAR(50) NOT NULL
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
);
ALTER TABLE user_state OWNER TO postgres;
GRANT ALL ON TABLE user_state TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE user_state TO webuser;

DROP TABLE IF EXISTS "user" CASCADE;
CREATE TABLE "user" (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , user_state_id UUID NOT NULL REFERENCES user_state(id)
  , username VARCHAR(25) NOT NULL
  , password VARCHAR(200) NOT NULL
  , token_reset_password UUID NULL
  , token_expiration TIMESTAMP WITHOUT TIME ZONE
  , admin BOOL NOT NULL DEFAULT FALSE
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
  , UNIQUE (username)
);
ALTER TABLE "user" OWNER TO postgres;
GRANT ALL ON TABLE "user" TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE "user" TO webuser;

DROP TABLE IF EXISTS user_profile CASCADE;
CREATE TABLE user_profile (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , user_id  UUID NOT NULL REFERENCES "user"(id)
  , first_name VARCHAR(50) NOT NULL
  , last_name VARCHAR(50)
  , image_url VARCHAR(250) NOT NULL
  , image_file_name VARCHAR(125) NOT NULL
  , image_details JSONB
  , description VARCHAR(500)
  , phone VARCHAR(50) NOT NULL
  , email VARCHAR(50) NOT NULL
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
  , UNIQUE (user_id)
);
ALTER TABLE user_profile OWNER TO postgres;
GRANT ALL ON TABLE user_profile TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE user_profile TO webuser;

------------------------------------------
------- CONTRACTOR TABLES ISSUES ---------
------------------------------------------

DROP TABLE IF EXISTS contractor CASCADE;
CREATE TABLE contractor (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , user_id  UUID NOT NULL REFERENCES "user"(id)
  , active BOOLEAN NOT NULL DEFAULT FALSE
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
);
ALTER TABLE contractor OWNER TO postgres;
GRANT ALL ON TABLE contractor TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE contractor TO webuser;

DROP TABLE IF EXISTS contractor_study CASCADE;
CREATE TABLE contractor_study (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , contractor_id  UUID NOT NULL REFERENCES contractor(id)
  , grade VARCHAR(250) NOT NULL
  , grade_date TIMESTAMP WITHOUT TIME ZONE NOT NULL
  , description VARCHAR(500)
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
);
ALTER TABLE contractor_study OWNER TO postgres;
GRANT ALL ON TABLE contractor_study TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE contractor_study TO webuser;


DROP TABLE IF EXISTS contractor_experience CASCADE;
CREATE TABLE contractor_experience (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , contractor_id  UUID NOT NULL REFERENCES contractor(id)
  , name VARCHAR(250) NOT NULL
  , description VARCHAR(500)
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
);
ALTER TABLE contractor_experience OWNER TO postgres;
GRANT ALL ON TABLE contractor_experience TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE contractor_experience TO webuser;


DROP TABLE IF EXISTS contractor_availability CASCADE;
CREATE TABLE contractor_availability (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4()
  , contractor_id  UUID NOT NULL REFERENCES contractor(id)
  , dates TIMESTAMP WITHOUT TIME ZONE [] NOT NULL
  , created_by UUID
  , updated_by UUID
  , created_dtm TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'UTC')
  , updated_dtm TIMESTAMP WITHOUT TIME ZONE
);
ALTER TABLE contractor_availability OWNER TO postgres;
GRANT ALL ON TABLE contractor_availability TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE contractor_availability TO webuser;
